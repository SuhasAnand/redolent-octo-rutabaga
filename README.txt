# redolent-octo-rutabaga
Repository that contains the list the tech blogs that I think are good reads for curious techies. 

1) The Architecture of Open Source Applications : http://aosabook.org/en/index.html //Thanks to Ajit for introducing me to this Site.

2) Dockers, containers & micro-services: http://sebgoa.blogspot.com/ //Author of Docker Cookbook

3) http://aosabook.org/en/500L/ & https://github.com/aosabook/500lines

4) CS183C: Technology-enabled Blitzscaling: The Visible Secret of Silicon Valley’s Success: https://www.youtube.com/playlist?list=PLnsTB8Q5VgnVzh1S-VMCXiuwJglk5AV--
or 
https://medium.com/notes-essays-cs183c-technology-enabled-blitzscalin

5) http://n00tc0d3r.blogspot.com/
